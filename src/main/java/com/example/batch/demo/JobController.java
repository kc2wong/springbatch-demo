package com.example.batch.demo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "/job")
public class JobController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Autowired
    @Qualifier("step1")
    private Step step;

//    @Autowired
//    private Job job;

    //    @Bean
//    protected Step step1(ItemReader<Transaction> reader,
//                         ItemProcessor<Transaction, Transaction> processor,
//                         ItemWriter<Transaction> writer) {
//        return steps.get("step1").<Transaction, Transaction> chunk(10)
//                .reader(reader).processor(processor).writer(writer).build();
//    }

//    @Bean(name = "firstBatchJob")
//    public Job job(@Qualifier("step1") Step step1) {
//        return jobs.get("firstBatchJob").start(step1).build();
//    }

    @PostMapping("/start")
    public void startJob() {

        System.out.println("Starting the batch job");

        String jobId = UUID.randomUUID().toString();
        Job job = jobs.get(jobId).start(step).build();

        try {
            JobExecution execution = jobLauncher.run(job, new JobParameters());
            System.out.println("Job Status : " + execution.getStatus());
            System.out.println("Job completed");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Job failed");
        }

    }
}
